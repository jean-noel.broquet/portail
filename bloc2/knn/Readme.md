# Algorithme des k plus proches voisins - K-NN

* [support de cours](cours_KNN.pdf)
* [exercices](exo-td-knn.md)
* [travaux pratiques](pokemon/readme.md)

# Pour télécharger des jeux de données :

* [UCI dataset](https://archive.ics.uci.edu/ml/datasets.php)
* [kaggle](https://www.kaggle.com/datasets)
