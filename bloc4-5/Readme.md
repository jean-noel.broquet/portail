blocs 4 et 5 — Programmation avancée et bases de données — Algorithmique avancée
================================================================================

[DIU Enseigner l'informatique au lycée](../Readme.md)

Lundi 4 novembre 2019
=====================

* Découverte du langage SQL
  - [travaux pratiques](sql1/Readme.md)
	- éléments de solution :
	  [reponses_exo1.sql](sql1/reponses_exo1.sql) et
	  [reponses_exo2.sql](sql1/reponses_exo2.sql)
  - [support de cours](sql1-cours/cours_sql1.md)

* La structure de données liste
  - [travaux pratiques](listes/readme.md)

Lundi 18 novembre 2019
======================

* Requêtes SQL en Python
  - [travaux pratiques](sql2/Readme.md)
	- éléments de solution :
	  [sql2_partie1.py](sql2/sql2_partie1.py) et
	  [sql2_partie2.py](sql2/sql2_partie2.py)

* La structure de données pile - un exemple d'utilisation
  - [travaux pratiques](pile_file/pile.md)

* Types de données abstrait et structures de données
  - [support de cours](sd/readme.md)

Lundi 2 décembre 2019
=====================

* Conception de bases de données relationnelles
  - [support de cours](sql2-cours/cours_sql2.md)
  - [travaux pratiques](sql3/Readme.md)
	- éléments de solution : [correction.md](sql3/correction.md)

* Introduction à la Programmation orientée objet
  - [support de cours et exemples](poo/readme.md)

* Arbres binaires
  - [support de travaux dirigés](arbres/arbre1.md)

Mercredi 18 décembre 2019
=========================

* Introduction à la Programmation orientée objet \
  suite et fin
  - [support de cours et exemples](poo/readme.md)

* Arbres binaires et arbres de recherches
  - [support de travaux dirigés et travaux pratiques](arbres/Readme.md)

Jeudi 19 décembre 2019
======================

* Compression
  - [support de cours](compression/compression.pdf)

* Codage Huffman
  - [support de travaux pratiques](huffman/Readme.md)
  
Mardi 7 janvier 2020
====================

* Graphes
  * [support de TD et TP](graphe/Readme.md)


Mercredi 8 janvier 2020
=======================

* [Projet _Icewalker_](icewalker/Readme.md)
* Recherche textuelle - Algorithmique du texte
  - [support de présentation](algo-texte/algo-texte.pdf)
