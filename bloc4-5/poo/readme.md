# introduction à la Programmation Orientée Objet

 - [support de cours](intro_poo.pdf)
 - [archive des codes source exemple](code_poo.zip)
 - [jupyter notebook pour `main_time.py`](code/main_time.ipynb)

On s'intéresse ici en particulier aux éléments suivants du programme de NSI de temrinale :

Dans la partie *Structures de données* :
<img src="./images/NSI-terminale-SD.png" width="700"/>

Dans la partie *Langages et programmation* :
<img src="./images/NSI-terminale-langages.png" width="700"/>

mais aussi dans la partie *Algorithmique* :

<img src="./images/NSI-terminale-algo-arbre.png" width="700"/>

<img src="./images/NSI-terminale-algo-graphe.png" width="700"/>

et pour ce qui concerne le DIU : 

<img src="./images/DIU-bloc4-langages-prog.png" width="450"/>
