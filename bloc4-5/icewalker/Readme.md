Projet _Icewalker_
==================

Évaluation - rendre votre travail
---------------------------------

Le travail réalisé dans le cadre de ce projet sera pris en compte dans l'évaluation.

Ce travail sera rendu via un dépôt GitLab ad hoc. Voyez les instructions à

* [gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker](https://gitlab-fil.univ-lille.fr/diu-eil-lil/icewalker)

Icewalker
---------

* le sujet [icewalker.md](icewalker.md)
* fichiers de configuration [./icewalker-grids.zip](./icewalker-grids.zip)

