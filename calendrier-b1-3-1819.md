Calendrier 
==========

[DIU Enseigner l'informatique au lycée](./Readme.md)

La présente page correspond au calendrier des blocs 1 à 3, 2018/19 —
pour mémoire.

Voir la [page calendrier](calendrier.md) pour d'autres éléments de calendrier. 


jeudi 9 mai 2019
================

C'est la rentrée ! 

| quand       | quoi                                                                                                                   | qui      | où            | avec qui                           |
|-------------|------------------------------------------------------------------------------------------------------------------------|----------|---------------|------------------------------------|
| 8h30        | café d'accueil                                                                                                         |          | hall du M5    |                                    |
| 9h          | amphi de rentrée - [support de présentation](doc/2019-06-diueil-slide.pdf) / [4 par page ](doc/2019-06-diueil-4up.pdf) |          | amphi Bacchus | Benoit Papegay et Philippe Marquet |
| 10h30-12h15 | [bloc 3](bloc3/Readme.md), TP Prise en main de l'environnement de travail                                              | groupe 1 | M5-A12        | Jean-Luc, Yvan                     |
|             |                                                                                                                        | groupe 2 | M5-A13        | Laurent, Philippe                  |
|             |                                                                                                                        | groupe 3 | M5-A14        | Benoit, Jean-François              |
| 13h45-17h   | [bloc 1](bloc1/Readme.md), Travaux pratiques                                                                           | groupe 1 | M5-A11        | Mikaël, Philippe                   |
|             |                                                                                                                        | groupe 2 | M5-A13        | Jean-Christophe, Patricia          |
|             |                                                                                                                        | groupe 3 | M5-A14        | Maude, Éric                        |

Accès au bâtiment M5 : [carte](https://osm.org/go/0B1fzL6bh--?m=) et [plan du campus](https://www.univ-lille.fr/fileadmin/user_upload/autres/Plan-site-Ulille-contact-cite%CC%81-scientifique.pdf)

* l'amphi Bacchus se situe au rdc du bâtiment M5
* l'amphi Cauchy se situe au bâtiment M1
* les salles A11 à A16 se situent au 1er étage du M5
* les salles A1 à A9 se situent au rdc du M5. 


vendredi 10 mai 2019
====================

| quand       | quoi                                  | qui      | où      | avec qui                 |
|-------------|---------------------------------------|----------|---------|--------------------------|
| 9h-10h30    | [bloc 2](bloc2/Readme.md), TD         | groupe 1 | M5-A2   | Marie-Émilie, Benoit     |
|             |                                       | groupe 2 | M5-A3   | Patricia, Bruno Bogaert          |
|             |                                       | groupe 3 | M5-A9   | Philippe, Lucien         |
| 10h45-12h15 | [bloc 2](bloc2/Readme.md), TP         | groupe 1 | M5-A16  | Marie-Émilie, Benoit     |
|             |                                       | groupe 2 | M5-A13  | Patricia, Bruno Bogaert          |
|             |                                       | groupe 3 | M5-A14  | Philippe, Lucien         |
| 13h45-14h45 | [bloc 3](bloc3/Readme.md), conférence |          | Bacchus | Gilles, Samuel, Philippe |
| 15h-17h     | [bloc 3](bloc3/Readme.md), TP         | groupe 1 | M5-A11  | Jean-Luc, Yvan           |
|             |                                       | groupe 2 | M5-A13  | Laurent, Philippe        |
|             |                                       | groupe 3 | M5-A14  | Benoit, Nicolas          |
	
mercredi 15 mai 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui               |
|-------------|-------------------------------|------------------|---------------|------------------------|
| 9h00-10h00  | cours                         | tous les groupes | amphi Bacchus | Éric                   |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TP | groupe 1         | A11           | Benoit, Jean-Stéphane  |
|             |                               | groupe 2         | A12           | Jean-Christophe,Fabien |
|             |                               | groupe 3         | A13           | Éric, Maude            |


après-midi - [bloc 2](bloc2/Readme.md)
-------------------

| quand       | quoi                          | qui      | où  | avec qui                |
|-------------|-------------------------------|----------|-----|-------------------------|
| 13h45-15h15 | [bloc 2](bloc2/Readme.md), TD | groupe 1 | A6  | Marie-Émilie, Benoit    |
|             |                               | groupe 2 | A7  | Patricia, Bruno Bogaert |
|             |                               | groupe 3 | A8  | Éric, Jean-Stéphane     |
| 15h30-17h00 | [bloc 2](bloc2/Readme.md), TP | groupe 1 | A11 | Marie-Émilie, Benoit    |
|             |                               | groupe 2 | A12 | Patricia, Bruno Bogaert |
|             |                               | groupe 3 | A13 | Éric, Jean-Stéphane     |


mercredi 5 juin 2019
====================

matin - [bloc 1](bloc1/Readme.md)
--------------

| quand       | quoi                          | qui              | où            | avec qui                  |
|-------------|-------------------------------|------------------|---------------|---------------------------|
| 9h00-10h00  | cours                         | tous les groupes | amphi Bacchus | Éric                      |
| 10h15-12h15 | [bloc 1](bloc1/Readme.md), TD | groupe 1         | A1            | Benoit, Mikaël            |
|             |                               | groupe 2         | A2            | Jean-Christophe, Patricia |
|             |                               | groupe 3         | A3            | Éric, Fabien              |


après-midi - [bloc 3](bloc3/Readme.md)
-------------------

| quand       | quoi                          | qui      | où            | avec qui              |
|-------------|-------------------------------|----------|---------------|-----------------------|
| 13h45-14h45 | cours                         |          | amphi Bacchus | Philippe              |
| 15h00-17h00 | [bloc 3](bloc3/Readme.md), TP | groupe 1 | A11           | Philippe, Thomas      |
|             |                               | groupe 2 | A12           | Laurent, Jean-Luc     |
|             |                               | groupe 3 | A13           | Benoit, Jean-Stéphane |


semaine du 17 juin 2019
=======================

du lundi au vendredi 

lundi 17 juin 2019
==================

matin : bloc 1
--------------

| quand       | quoi                                 | qui              | où            | avec qui        |
|-------------|--------------------------------------|------------------|---------------|-----------------|
| 9h00-10h00  | cours                                | tous les groupes | amphi Bacchus | Jean-Christophe |
| 10h15-12h15 | [bloc1](bloc1/Readme.md#17-juin), TP | groupe 1         | A11           | Benoit          |
|             |                                      | groupe 2         | A12           | Jean-Christophe |
|             |                                      | groupe 3         | A13           | Fabien          |
|             |                                      |                  |               | Philippe        |



après-midi : bloc 2
-------------------
  
| quand       | quoi             | qui           | où            | avec qui                |
|-------------|------------------|---------------|---------------|-------------------------|
| 13h45-14h45 | cours            | tout le monde | amphi Bacchus | Éric                    |
| 15h00-17h00 | travaux dirigés  | groupe 1      | A1            | Marie-Émilie, Benoit    |
|             |                  | groupe 2      | A2            | Patricia, Bruno Bogaert |
|             |                  | groupe 3      | A3            | Éric, Jean-Stéphane     |
  
mardi 18 juin 2019
==================

matin : bloc 1
--------------

| quand       | quoi                                 | qui              | où            | avec qui         |
|-------------|--------------------------------------|------------------|---------------|------------------|
| 9h00-10h00  | cours                                | tous les groupes | amphi Bacchus | Jean-Christophe  |
| 10h15-12h15 | [bloc1](bloc1/Readme.md#18-juin), TD | groupe 1         | A1            | Patricia, Mikaël |
|             |                                      | groupe 2         | A2            | JC, Fabien       |
|             |                                      | groupe 3         | A3            | Philippe, Maude  |
  		
		
après-midi : bloc 2
-------------------

| quand       | quoi  | qui           | où            | avec qui                |
|-------------|-------|---------------|---------------|-------------------------|
| 13h30-14h30 | cours | tout le monde | amphi Bacchus | Sylvain                 |
| 14h45-16h45 | TD    | groupe 1      | A1            | Marie-Émilie            |
|             |       | groupe 2      | A2            | Patricia, Bruno Bogaert |
|             |       | groupe 3      | A3            | Éric, Laetitia          |
  

mercredi 19 juin 2019
=====================

matin : [bloc1](bloc1/Readme.md#19-juin)
--------------

| quand      | quoi | qui      | où  | avec qui         |
|------------|------|----------|-----|------------------|
| 9h00-12h15 | TP   | groupe 1 | A11 | Patricia, Benoit |
|            |      | groupe 2 | A12 | Mikaël, JC       |
|            |      | groupe 3 | A13 | Maude, Philippe  |
  
après-midi : [bloc 3, réseau](bloc3/Readme.md#mercredi-19-juin-2019)
-------------------

| quand       | quoi  | qui           | où    | avec qui         |
|-------------|-------|---------------|-------|------------------|
| 13h15-14h15 | cours | tout le monde | amphi | Thomas           |
| 14h30-16h30 | TP    | groupe 1      | A11   | Yvan             |
|             |       | groupe 2      | A12   | Xavier, Jean-Luc |
|             |       | groupe 3      | A13   | Thomas           |

jeudi 20 juin 2019
==================

matin : [bloc1](bloc1/Readme.md#20-juin)
--------------

| quand       | quoi  | qui           | où            | avec qui       |
|-------------|-------|---------------|---------------|----------------|
| 9h00-10h00  | cours | tout le monde | amphi Bacchus | Mikaël         |
| 10h15-12h15 | TP    | groupe 1      | A11           | Lucien         |
|             |       | groupe 2      | A12           | Éric, Jean-Luc |
|             |       | groupe 3      | A13           | Benoit         |

après-midi : [bloc 3](bloc3/Readme.md#jeudi-20-juin-2019)
-------------------

| quand       | quoi       | qui           | où    | avec qui         |
|-------------|------------|---------------|-------|------------------|
| 13h15-14h15 | Conférence | tout le monde | amphi | Bruno / Philippe |
| 14h30-16h30 | TP         | groupe 1      | A11   | Pierre           |
|             |            | groupe 2      | A12   | Xavier           |
|             |            | groupe 3      | A13   | Thomas           |


vendredi 21 juin 2019
=====================

[bloc 2](bloc2/Readme.md#séance-5-21-juin), [conférence](Readme.md#-enjeux-environnentaux), et [qcm](Readme.md#-qcm-classe-de-1re)
--------------

| quand       | quoi  | qui              | où            | avec qui                |
|-------------|-------|------------------|---------------|-------------------------|
| 9h00-10h00  | cours bloc 2 | tous les groupes | amphi Bacchus | Sylvain                 |
| 10h15-11h15 | [conférence enjeux environnementaux du numérique](Readme.md#-enjeux-environnentaux) | tous | amphi Bacchus | Peter Sturm |
| 11h15-12h15 | TP    | groupe 1         | A11           | Laetitia, Marie-Émilie            |
|             |       | groupe 2         | A12           | Bruno Bogaert           |
|             |       | groupe 3         | A14           | Jean-Stéphane |
| 13h15-14h45 | suite TP bloc 2  | tous | A11-A12-A14      | |
| 15h00-16h30 | [travail QCM](Readme.md#-qcm-classe-de-1re)    | en petits groupes | A1-A3| Benoit, Jean-Christophe,  Laetitia, Patricia, Philippe |

* À partir de 16h30, présentation, exposition/démonstration par les
  jeunes filles de l'action _L codent L créent_ - amphi Bacchus et
  Hall du M5. 
  - [flyer d'invitation](img/2019-06-lclc-expo-invit.png)

semaine du 1er juillet 2019
===========================

* du lundi au jeudi
* projet - transversal aux blocs 1 et 2
* bloc 2
* bloc 3

lundi 1er juillet 2019
======================

matin : [bloc 2](bloc2/Readme.md#séance-6-et-7-semaine-du-1er-juillet)
--------

| quand       | quoi  | qui              | où           | avec qui              |
|-------------|-------|------------------|--------------|-----------------------|
| 9h15-10h15  | cours | tous les groupes | amphi Cauchy | Laetitia              |
| 10h30-12h15 | TD/TP | groupe 1         | A11          | Laetitia              |
|             |       | groupe 2         | A12          | Bruno, Patricia      |
|             |       | groupe 3         | A14          | Benoit, Jean-Stéphane |

après-midi : [projet bloc 1 et 2](bloc1/Readme.md#1er-juillet)
--------

| quand       | quoi  | qui              | où           | avec qui        |
|-------------|-------|------------------|--------------|-----------------|
| 13h15-14h15 | cours | tous les groupes | amphi Cauchy | Jean-Christophe |
| 14h30-16h30 | TP    | groupe 1         | A11          | Benoit          |
|             |       | groupe 2         | A12          | JC, Mikaël      |
|             |       | groupe 3         | A14          | Maude           |

mardi 2 juillet 2019
====================

matin : [projet bloc 1 et 2](bloc1/Readme.md#1er-juillet)
--------

| quand      | quoi | qui      | où  | avec qui |
|------------|------|----------|-----|----------|
| 9h00-12h15 | TP   | groupe 1 | A11 | Benoit   |
|            |      | groupe 2 | A12 | JC       |
|            |      | groupe 3 | A14 | Patricia |

après-midi : [bloc3](bloc3/Readme.md#mardi-2-juillet)
--------

| quand       | quoi       | qui              | où           | avec qui       |
|-------------|------------|------------------|--------------|----------------|
| 13h15-14h15 | conférence | tous les groupes | amphi Cauchy | Bruno/Philippe |
| 14h30-16h30 | TD         | groupe 1         | A9           | Philippe       |
|             |            | groupe 2         | A8           | Jean-Luc       |
|             |            | groupe 3         | A7           | Benoit         |

mercredi 3 juillet 2019
=======================

matin :  [bloc 2](bloc2/Readme.md#séance-6-et-7-semaine-du-1er-juillet)
--------

| quand      | quoi | qui      | où  | avec qui              |
|------------|------|----------|-----|-----------------------|
| 9h00-12h15 | TP   | groupe 1 | A11 | Patricia, Benoit      |
|            |      | groupe 2 | A12 | Bruno, Philippe       |
|            |      | groupe 3 | A14 | Jean-Stéphane, Éric   |

après-midi : [bloc3](bloc3/Readme.md#mercredi-3-juillet)
--------

| quand       | quoi  | qui              | où           | avec qui |
|-------------|-------|------------------|--------------|----------|
| 13h15-14h15 | cours | tous les groupes | amphi Cauchy | Philippe |
| 14h30-16h30 | TD    | groupe 1         | A9           | Philippe |
|             |       | groupe 2         | A8           | Jean-Luc |
|             |       | groupe 3         | A7           | Benoit   |

jeudi 4 juillet 2019
====================

* 11h : conférence métiers de l'informatique, amphi Cauchy
  * Catherine Lherbier (directrice Agence Nord, Quadra Informatique)
  * membre de l'AD2N [ad2n.org](https://www.ad2n.org/presentation/),
	association régionale d'entreprises de services du numérique (ESN)
	et éditeurs de logiciels.

[projet, suite et fin](bloc1/Readme.md#1er-juillet)
--------------------


| quand       | quoi  | qui              | où           | avec qui         |
|-------------|-------|------------------|--------------|------------------|
| 9h-11h00    | TP    | groupe 1         | A11          | Benoit, Mikaël   |
|             |       | groupe 2         | A12          | JC, Maude        |
|             |       | groupe 3         | A14          | Éric, Philippe   |
| 13h15-14h   | jupyter | ceux qui veulent | amphi Cauchy | Éric     |
| 13h15-16h30 | TP    | groupe 1         | A11          | Benoit, Mikaël   |
|             |       | groupe 2         | A12          | JC, Maude        |
|             |       | groupe 3         | A14          | Éric, Philippe   |

